__author__ = 'AndresFelipe'

import unittest
from Calc import Calc


class CalcTestCase(unittest.TestCase):
    def test_suma_vacio(self):
        self.assertEqual(Calc().suma(""), 0, "String Vacio")

    def test_suma_un_numero(self):
        self.assertEqual(Calc().suma("1"), 1, "String Un Numero")
        self.assertEqual(Calc().suma("3"), 3, "String Un Numero")

    def test_suma_dos_numeros(self):
        self.assertEqual(Calc().suma("1,2"), 3, "String Dos Numeros")
        self.assertEqual(Calc().suma("12,15"), 27, "String Dos Numeros")

    def test_suma_mas_numeros(self):
        self.assertEqual(Calc().suma("1,2,3"), 6, "String Mas Numeros")
        self.assertEqual(Calc().suma("12,15,3"), 30, "String Mas Numeros")
        self.assertEqual(Calc().suma("12,15"), 27, "String Mas Numeros")

    def test_suma_otros_separadores(self):
        self.assertEqual(Calc().suma("1,2,3"), 6, "String Otros Separadores")
        self.assertEqual(Calc().suma("1:2:3"), 6, "String Otros Separadores")
        self.assertEqual(Calc().suma("1&2&3"), 6, "String Otros Separadores")
        self.assertEqual(Calc().suma("12,15,3,1"), 31, "String Otros Separadores")
        self.assertEqual(Calc().suma("12:15:3:1"), 31, "String Otros Separadores")
        self.assertEqual(Calc().suma("12&15&3&1"), 31, "String Otros Separadores")
        self.assertEqual(Calc().suma("12,15"), 27, "String Otros Separadores")
        self.assertEqual(Calc().suma("12:15"), 27, "String Otros Separadores")
        self.assertEqual(Calc().suma("12&15"), 27, "String Otros Separadores")
        self.assertEqual(Calc().suma("12:15&3,1"), 31, "String Otros Separadores")

if __name__ == '__main__':
    unittest.main()
